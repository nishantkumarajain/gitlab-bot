var GitLabStrategy = require('passport-gitlab2').Strategy;
var authConfig = require('./auth.config.js');
var db = require('../utils/mongo.util.js').getDB();
var ObjectId = require('mongodb').ObjectID;

module.exports = function(passport) {

	passport.serializeUser(function(user,done) {
		done(null,user._id);
	});

	passport.deserializeUser(function(id,done) {
		db.collection('gitlabAccounts').find({_id:ObjectId(id)}).toArray(function(err,user){
			done(err,user);
		});
	});

	passport.use(new GitLabStrategy({
		clientID: authConfig.gitlabAuth.clientID,
		clientSecret: authConfig.gitlabAuth.clientSecret,
		callbackURL: authConfig.gitlabAuth.callbackURL,
		passReqToCallback: true
	},function(req,accessToken,refreshToken,profile,done) {
		console.log(accessToken);
		req.body.profileId = profile._json.id;
		req.body.accessToken = accessToken;
		db.collection('gitlabAccounts').find({profileId:profile._json.id,email:profile._json.email}).toArray(function(err,profiles){
			if(err)
				done(err);

			if(profiles.length > 0) {
				done(null,profiles[0]);
			} else {
				var profileToBeInserted = {
					profileId: profile._json.id,
					email: profile._json.email,
					accessToken: accessToken
				};
				db.collection('gitlabAccounts').insertOne(profileToBeInserted,function(err,profile) {
					done(null,profile.ops[0]);
				});
			}
		});
	}));
};