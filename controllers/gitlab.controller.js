var gitlabConfig = require('../config/gitlab.config.js');
module.exports = function(accessToken,resource,method,args) {
	var gitlab = require('gitlab')({
		url: gitlabConfig.url,
		oauth_token: accessToken
	});
	console.log(args)
	gitlab[resource][method].apply(null,[args]);
};