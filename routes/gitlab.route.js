module.exports = function(app,passport) {
	
	app.get('/auth/gitlab',passport.authenticate('gitlab',{
		scope: ['api']
	}));

	app.get('/auth/gitlab/callback', passport.authenticate('gitlab',{
		failureRedirect: '/login'
	}),function(req,res) {
		try {
			var oauth_token = req.body.accessToken;
			require('../controllers/gitlab.controller.js')(oauth_token,'projects','all',function(projects) {
				console.log(projects);
			})
		} catch(err) {
			console.log(err);
		}

		res.redirect('/index.html');
	});

}