var MongoClient = require('mongodb').MongoClient;
var _db;

module.exports = {
	getConnection: function(url,callback) {
		MongoClient.connect(url,function(err,db) {
			_db = db;
			callback(err,db);
		})
	},
	getDB: function() {
		return _db;
	}
};
